import React, {useState} from "react";
import './styles.css'

const MessageInput = ({submitMsg, isEditAction}) => {
    const [newMsg, setNewMsg] = useState()
    return (
        <div className={'message-input'}>
            <textarea onChange={e => setNewMsg(e.target.value)}/>
            <button onClick={() => {
                submitMsg(newMsg);
                setNewMsg('')
            }}> Submit
            </button>
        </div>
    )

};
export default MessageInput;
