export const formatDate = (date, dateFormat) => {
    if (dateFormat){
        return new Intl.DateTimeFormat('en-US', dateFormat).format(new Date(Date.parse(date)))

    }else{
        return new Intl.DateTimeFormat('en-US', {
            year: 'numeric',
            month: 'short',
            day: "numeric",
            hour: "2-digit",
            minute: "2-digit"
        }).format(new Date(Date.parse(date)))

    }
}

export const equalDates = (date1, date2) => {
    const d1 = new Date(date1)
    const d2 = new Date(date2)
    // console.log(d1.getDay() === d2.getDay(), d1.getMonth() === d2.getMonth(), d1.getFullYear() === d2.getFullYear())
    return d1.getDay() === d2.getDay() && d1.getMonth() === d2.getMonth() && d1.getFullYear() === d2.getFullYear()
}
export const isToday = (date1) => {
    const d1 = new Date(date1)
    const d2 = new Date()
    // console.log(d1.getDay() === d2.getDay(), d1.getMonth() === d2.getMonth(), d1.getFullYear() === d2.getFullYear())
    return d1.getDay() === d2.getDay() && d1.getMonth() === d2.getMonth() && d1.getFullYear() === d2.getFullYear()
}
