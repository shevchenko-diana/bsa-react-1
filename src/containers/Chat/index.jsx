import React from "react";
import Header from "../../components/Header";
import MessageList from "../../components/MessageList";
import MessageInput from "../../components/MessageInput";
import './styles.css'
import Spinner from "../../components/Spinner";
import {v4 as uuidv4} from 'uuid';

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            ownUser: {},
            chatInfo: {},
            messages: []
        }
        this.fetchMessages = this.fetchMessages.bind(this)
        this.appendNewMsg = this.appendNewMsg.bind(this)
        this.likeMsg = this.likeMsg.bind(this)
    }

    componentWillMount() {
        this.fetchMessages()
    }

    fetchMessages = () => {
        fetch('https://api.npoint.io/b919cb46edac4c74d0a8')
            .then(res => res.json())
            .then(messages => {
                //  chatName: name, numOfUsers, numOfMessages,
                //                     lastMsgTimestamp: timestamp
                const users = new Set()
                messages.forEach(msg => users.add(msg.userId))
                const chatInfo = {
                    numOfUsers: users.size,
                    timestamp: messages[messages.length - 1].createdAt,
                    numOfMessages: messages.length,
                    chatName: 'Launderers'
                }

                console.log(messages)
                this.setState({
                    messages: messages,
                    chatInfo: chatInfo,
                    isLoading: false,
                    ownUser: {
                        userId: messages[0].userId,
                        user: messages[0].user,
                    },
                })
            })
    }

    appendNewMsg = text => {
        const newMsg = {
            createdAt: new Date().toISOString(),
            editedAt: '',
            id: uuidv4(),
            text,
            user: this.state.ownUser.user,
            userId: this.state.ownUser.userId,
        }

        this.setState({
            messages: [...this.state.messages, newMsg],
            chatInfo: {
                ...this.state.chatInfo,
                numOfMessages: this.state.messages.length + 1,
                timestamp: newMsg.createdAt
            }
        })

    }

    likeMsg = id => {
        const messages = this.state.messages.map(msg => {
            if (msg.id === id) {
                msg.isLiked = !msg.isLiked
            }
            return msg
        })


        this.setState({
            messages: messages
        })
    }
    editMsg = (id, text) => {
        const messages = this.state.messages.map(msg => {
            if (msg.id === id) {
                msg.text = text
            }
            return msg
        })


        this.setState({
            messages: messages
        })
    }
    deleteMsg = (id) => {
        const messages = this.state.messages.filter(msg => msg.id !== id)


        this.setState({
            messages: messages
        })
    }

    render() {
        return (
            this.state.isLoading ? <Spinner/> :
                <div className={'chat-container'}>

                    <Header {...this.state.chatInfo}/>
                    <MessageList
                        messages={this.state.messages}
                        ownUserId={this.state.ownUser.userId}
                        likeMsg={this.likeMsg}
                        editMsg={this.editMsg}
                        deleteMsg={this.deleteMsg}
                    />
                    <MessageInput submitMsg={this.appendNewMsg}/>
                </div>
        )
    }
}


export default Chat;


